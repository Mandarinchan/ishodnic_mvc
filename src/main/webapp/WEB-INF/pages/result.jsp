<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
  <title>Developer Info</title>
</head>
<body>

<h2>Developer Information</h2>
<table>
  <tr>
    <td>Id</td>
    <td>${id}</td>
  </tr>
  <tr>
  <tr>
    <td>First Name</td>
    <td>${firstname}</td>
  </tr>
  <tr>
    <td>Last Name</td>
    <td>${lastname}</td>
  </tr>
  <tr>
    <td>Middle Name</td>
    <td>${middlename}</td>
  </tr>
</table>
</body>
</html>