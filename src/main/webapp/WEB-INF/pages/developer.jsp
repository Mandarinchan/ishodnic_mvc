<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
  <title>Developer</title>
</head>
<body>

<h2>Enter developer information</h2>
<form:form method="post" action="addDeveloper">
  <table>
    <tr>
      <td><form:label path="id">Id</form:label></td>
      <td><form:input path="id" /></td>
    </tr>
    <tr>
      <td><form:label path="firstname">First Name</form:label></td>
      <td><form:input path="firstname" /></td>
    </tr>
    <tr>
      <td><form:label path="firstname">Last Name</form:label></td>
      <td><form:input path="lastname" /></td>
    </tr>
    <tr>
      <td><form:label path="firstname">Middle Name</form:label></td>
      <td><form:input path="middlename" /></td>
    </tr>
    <tr>
      <td colspan="2">
        <input type="submit" value="Submit"/>
      </td>
    </tr>
  </table>
</form:form>
</body>
</html>